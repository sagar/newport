#!/bin/sh

# Add the correct paths to test executables and libraries
. common/update-test-path

testname="newport-test"

# Call the binary
newport-client --tap
ret=$?

# Check return code
if [ $ret -eq 1 ] ; then
    echo "${testname}: pass"
else
    echo "${testname}: fail"
    exit 1
fi
